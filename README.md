# Description

Ansible role to install [Diana](https://github.com/metno/diana) and
[Tseries](https://github.com/metno/tseries). This does not fully
configure a meteorological workstation.
